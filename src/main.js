import Vue from 'vue'
import App from './App.vue'
import Header from './assets/components/common/Header.vue'
import carousel from './assets/components/common/carousel.vue'
import ContactForm1 from './assets/components/common/ContactForm1.vue'
import googlemap from './assets/components/common/googlemap.vue'
import footer from './assets/components/common/footer.vue'
import projectslider from './assets/components/common/projectslider.vue'
import clientsMessage from './assets/components/homepage/clientsMessage.vue'
import contactusMap from './assets/components/common/contactusMap.vue'
import pagebanner from './assets/components/common/pagebanner.vue'
import contentdigitalbanner from './assets/components/common/contentdigitalbanner.vue'
import VueCarousel from 'vue-carousel'
import Homepage from './assets/components/Pages/Homepage.vue'
import creativeNcontentsecondSec from './assets/components/creativeNcontent/creativeNcontentsecondSec.vue'
import VueIconFont from 'vue-icon-font'
import * as VueGoogleMaps from 'vue2-google-maps'
import secondpanel from './assets/components/homepage/secondpanel.vue'
import router from './router';
import BootstrapVue from 'bootstrap-vue'
import AOS from 'aos'
import VueAnalytics from 'vue-analytics'
import swal from 'sweetalert2'
import 'aos/dist/aos.css'
import './assets/styles/flickity.css'

Vue.use(BootstrapVue);
Vue.component ('projslider', projectslider)
Vue.component ('HomeGWF', Homepage)
Vue.component ('digiconbanner', contentdigitalbanner)
Vue.component ('gwfbanner', pagebanner)
Vue.component('contact-us', ContactForm1)
Vue.component('question-list', creativeNcontentsecondSec)
Vue.component('gwffooter', footer)
Vue.component('gmap', googlemap)
Vue.component('conmap', contactusMap)
Vue.component('msgcli', clientsMessage)
Vue.component('secondpanelHome', secondpanel)
Vue.component('headergwf', Header)
Vue.component('carouselgwf', carousel)
Vue.use(VueCarousel);
Vue.use(VueIconFont);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

Vue.use(VueAnalytics, {
  id: 'UA-44051664-1'
})
// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');
    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => jQuery(tag).insertAfter(jQuery('title')));

  next();
});
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router: router,
  template: '<App/>',
  components: { App },
});

