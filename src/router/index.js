import Vue from 'vue'
import Router from 'vue-router'
import Home from '../assets/components/Pages/Homepage'
import About from '../assets/components/Pages/Aboutus'
import TermsAndConditions from '../assets/components/Pages/TermsAndConditions'
import Privacy from '../assets/components/Pages/Privacy.vue'
import Contact from '../assets/components/Pages/Contactus'
import CreativeContent from '../assets/components/Pages/CreativeandContent'
import DigitalMarketing from '../assets/components/Pages/Digitalmarketing'
import ContentAndDigital from '../assets/components/Pages/ContentAndDigital'
import CreativeGraphics from '../assets/components/Pages/CreativeGraphics'
import Portfolio from '../assets/components/Pages/Portfolio'
import WebDesign from '../assets/components/Pages/WebDesign'
import BestWorks from '../assets/components/portfolio/BestWorks'
import Slider from '../assets/components/common/Slider'
import WebForceAu from '../assets/components/landingpages/pages/Fiveyears'
import Protect2018 from '../assets/components/landingpages/pages/Protect2018'
import ChineseNewYear from '../assets/components/landingpages/pages/ChineseNewYear'
import ValentinesDay from '../assets/components/landingpages/pages/Valentines'
import EntrepreneurFranchise from '../assets/components/landingpages/pages/Entrepreneur'
import Furniture from '../assets/components/landingpages/pages/Furniture'
import WorldBEX from '../assets/components/landingpages/pages/WorldBex'
import Subic from '../assets/components/landingpages/pages/Subic'
import Philippines from '../assets/components/landingpages/pages/Philippines'
import ManilaFame from '../assets/components/landingpages/pages/manilafameLP'
import Bacolod from '../assets/components/landingpages/pages/bacolod'
import HubSpot from '../assets/components/landingpages/pages/hubspot'

Vue.use(Router);
let host_uri = location.protocol+"//"+window.location.hostname


export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Global WebForce: Digital Agency in Melbourne',
        metaTags: [
          {
            name: 'description',
            content: 'We are a digital agency in Melbourne that services a diverse cliente across Australia, Asia & the Pacific, UK and the USA with digital services.'
          },
          {
            property: 'og:description',
            content: 'We are a digital agency in Melbourne that services a diverse cliente across Australia, Asia & the Pacific, UK and the USA with digital services.'
          }
        ]
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        title: 'About Global WebForce',
        metaTags: [
          {
            name: 'description',
            content: 'We are a digital agency in Melbourne with over 5 years of experience when it comes to delivering web and digital services for all business sizes.'
          },
          {
            property: 'og:description',
            content: 'We are a digital agency in Melbourne with over 5 years of experience when it comes to delivering web and digital services for all business sizes.'
          }
        ]
      }
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact,
      meta: {
        title: 'Contact Global WebForce',
        metaTags: [
          {
            name: 'description',
            content: 'To get started with your business website or to outsource digital projects, contact Global WebForce today,'
          },
          {
            property: 'og:description',
            content: 'To get started with your business website or to outsource digital projects, contact Global WebForce today,'
          }
        ]
      }
    },
    {
      path: '/digital-marketing',
      name: 'DigitalMarketing',
      component: DigitalMarketing,
      meta: {
        title: 'Outsource SEO, PPC and Social Media | Global WebForce',
        metaTags: [
          {
            name: 'description',
            content: 'Outsource Integrated marketing for businesses that outsource their digital marketing. We utilize alignment and strategy for the best results.'
          },
          {
            property: 'og:description',
            content: 'Outsource Integrated marketing for businesses that outsource their digital marketing. We utilize alignment and strategy for the best results.'
          }
        ]
      }
    },
    {
      path: '/design-and-content',
      name: 'ContentAndDigital',
      component: ContentAndDigital,
      meta: {
        title: 'Outsource Digital Design & Content Writing | Global WebForce',
        metaTags: [
          {
            name: 'description',
            content: 'Outsource all your design and content needs with Global WebForce, your one stop shop for all things digital and written.'
          },
          {
            property: 'og:description',
            content: 'Outsource all your design and content needs with Global WebForce, your one stop shop for all things digital and written.'
          }
        ]
      }
    },
    {
      path: '/creative-graphics',
      name: 'CreativeGraphics',
      component: CreativeGraphics,
    },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio,
      meta: {
        title: 'Global WebForce Digital Portfolio',
        metaTags: [
          {
            name: 'description',
            content: 'Check out our finest in works inside our digital portfolio.'
          },
          {
            property: 'og:description',
            content: 'Check out our finest in works inside our digital portfolio.'
          }
        ]
      }
    },
    {
      path: '/website-design',
      name: 'WebDesign',
      component: WebDesign,
      meta: {
        title: 'Website Design Packages | Global WebForce',
        metaTags: [
          {
            name: 'description',
            content: 'Need a business website? We offer full-suite website design packages fit for all business sizes with a stress free experience.'
          },
          {
            property: 'og:description',
            content: 'Need a business website? We offer full-suite website design packages fit for all business sizes with a stress free experience.'
          }
        ]
      }
    },
    // {
    //   path: '/five-years',
    //   name: 'Five Years',
    //   component: WebForceAu,
    //   meta: {
    //     title: 'Global WebForce Philippines 5 Years Anniversary Promo'
    //   }
    // },
    {
      path: '/protect-2018',
      name: 'Protect 2018',
      component: Protect2018,
      meta: {
        title: 'Protect 2018'
      }
    },
    {
      path: '/chinese-new-year',
      name: 'Chinese New Year',
      component: ChineseNewYear,
      meta: {
        title: 'Chinese New Year'
      }
    },
    {
      path: '/appointment/bacolod',
      name: 'Hub-Spot',
      component: HubSpot,
      meta: {
        title: 'Set an Appointment'
      }
    },
    {
      path: '/privacy-policy',
      name: 'Privacy',
      component: Privacy,
      meta: {
        title: 'Privacy Policy',
        metaTags: [
          {
            name: 'description',
            content: ''
          },
          {
            property: 'og:description',
            content: ''
          }
        ]
      }
    },
      {
          path: '/terms-and-conditions',
          name: 'Terms & Conditions',
          component: TermsAndConditions,
          meta: {
              title: 'Terms & Conditions',
              metaTags: [
                  {
                      name: 'description',
                      content: ''
                  },
                  {
                      property: 'og:description',
                      content: ''
                  }
              ]
          }
      },
    {
      path: '/valentines-day',
      name: 'ValentinesDay',
      component: ValentinesDay,
      meta: {
        title: 'Valentines Day'
      }
    },
    {
      path: '/entrepreneur-and-franchise',
      name: 'EntrepreneurFranchise',
      component: EntrepreneurFranchise,
      meta: {
        title: 'Entrepreneur & Franchise'
      }
    },
    {
      path: '/furniture',
      name: 'Furniture',
      component: Furniture,
      meta: {
        title: 'Furniture'
      }
    },
    {
      path: '/worldbex',
      name: 'WorldBEX',
      component: WorldBEX,
      meta: {
        title: 'WorldBEX'
      }
    },
    {
      path: '/subic',
      name: 'Subic',
      component: Subic,
      meta: {
        title: 'Subic'
      }
    },
    {
      path: '/philippines',
      name: 'Philippines',
      component: Philippines,
      meta: {
        title: 'Philippines'
      }
    },
    {
      path: '/manila-fame',
      name: 'ManilaFame',
      component: ManilaFame,
      meta: {
        title: 'Manila FAME 2018 BUSINESS WEBSITE PACKAGE',
        metaTags: [
          {
            property: 'description',
            content: 'Special offer for businesses that are in the furniture and handicraft industry. Get a brand new modern business website with our exclusive offer!'
          },
          {
            property: 'og:title',
            content: 'Manila FAME 2018 BUSINESS WEBSITE PACKAGE'
          },
          {
            property: 'og:description',
            content: 'Special offer for businesses that are in the furniture and handicraft industry. Get a brand new modern business website with our exclusive offer!'
          },
          {
            property: 'og:image',
            content: 'http://dev.globalwebforce.com/static/img/LP/manila/manila-fame-fb-graphics.jpg'
          },
          {
            property: 'og:image:type',
            content: 'image/jpeg'
          }
        ]
      }
    },
    {
      path: '/bacolod',
      name: 'Bacolod',
      component: Bacolod,
      meta: {
        title: ' Global WebForce in Bacolod: May 22 - May 26, 2018 ',
        metaTags: [
          {
            property: 'description',
            content: 'Special offer for businesses that are in the furniture and handicraft industry. Get a brand new modern business website with our exclusive offer!'
          },
          {
            property: 'og:title',
            content: ' Global WebForce in Bacolod: May 22 - May 26, 2018 '
          },
          {
            property: 'og:description',
            content: 'Special offer for businesses that are in the furniture and handicraft industry. Get a brand new modern business website with our exclusive offer!'
          },
          {
            property: 'og:image',
            content: 'http://dev.globalwebforce.com/static/img/LP/manila/manila-fame-fb-graphics.jpg'
          },
          {
            property: 'og:image:type',
            content: 'image/jpeg'
          }
        ]
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history'
});
