import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import Page from '@/components/site/Page';
import Services from '@/components/site/Services';

Vue.use(Router);
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Page',
      component: Page,
    },
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld,
    },
    {
      path: '/services',
      name: 'Services',
      component: Services,
    },
  ],
});
