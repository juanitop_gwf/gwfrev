<?php
    header('Access-Control-Allow-Origin: *');
    $to = "jake.sorianoo@gmail.com"; // this is your Email address
    $to2 = "got.shiit@gmail.com";
    $from = $_POST['cliEmail']; // this is the sender's Email address
    $clientname = $_POST['cliName'];
    $clientEmail = $_POST['cliEmail'];
    $clientContact = $_POST['cliContact'];
    $clientWebsite = $_POST['cliWebsite'];
    $list = $_POST['inclusions'];
    $subject = "Digital Marketing";
    $package = $_POST['package'];
    $subject2 = "Copy of your form submission";
    $listForEmail = '';
    for($i = 1; $i <= count($list); $i++) {
        $listForEmail .= "\n".$list[$i];
    }
    $message = `
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td width="730" align="center" valign="top" style="padding-right:30px;padding-left:30px">
              <img src="http://philippinetours.com.au/wp-content/uploads/2017/09/email_banner.jpg" alt="">
            </td>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding-top: 30px; padding-bottom: 30px; padding-right:30px;padding-left:30px; font-family: Arial;">
              <table align="center" border="0" cellpadding="0" cellspacing="0" width="640" style="font-family: Arial;">
                <tr>
                  <td width="640px">
                    <h4>Hi [your-name],</h4>
                    <p>
                      Thanks for reaching out. We have received your message sent through our website and we will respond to it as quickly as possible. You may also reach us on 03 5259 1110 for a faster response or to follow up on your enquiry.
                    </p>
                    <p>
                      Kind regards,
                    </p>
                    <p>
                      The Philippine Tours Team
                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center" valign="top"  style="padding-right:30px;padding-left:30px;">
              <table border="0" cellpadding="0" cellspacing="0" width="730">
                <tbody>
                  <tr>
                    <td bgcolor="#034ea2" align="center" valign="top" style="font-size:12px; font-weight:400; line-height:24px; padding-top:20px; padding-bottom:20px; text-align:center; color:#fff;  font-family: Arial;">
                      <p style="color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center">225 Melville Road Brunswick West, VIC, 3055 Australia</p>
                      <p style="color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center">Phone: <a href="tel:61393841844" style="color: #fff200; text-decoration: underline;">+61 3 9384 1844</a> | Email: <a href="mailto:sales@philippinetours.com.au " style="color: #fff200; text-decoration: underline;">sales@philippinetours.com.au </a> </p>
                      <p>Like us on <a href="https://www.facebook.com/philtoursAU/"  style="color: #fff200; text-decoration: underline;">Facebook</a></p>
                      <p style="color:#fff;" style="color:#fff;">Copyright ©2017 Philippine Tours. All Rights Reserved.</p>
                      <p>
                        <a href="http://philippinetours.com.au/" style="color:#fff;text-decoration:underline"  target="_blank" style="color:#fff;text-decoration:underline" target="_blank" style="color:#fff200;text-decoration:underline" target="_blank" ="">About Philippine Tours |</a>
                        <a href="http://philippinetours.com.au/contact" style="color:#fff;text-decoration:underline"  target="_blank" style="color:#fff;text-decoration:underline" target="_blank" ="" style="color:#fff200;text-decoration:underline" target="_blank" ="">Contact Us |</a>
                        <a href="http://phtours.webforceconceptwebsite.com.au/privacy-policy" style="color:#fff;text-decoration:underline" target="_blank"style="color:#fff200;text-decoration:underline" target="_blank" ="" style="color:#fff;text-decoration:underline"  target="_blank" ="">Privacy Policy</a>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    `

    $headers = "From:" . $from;
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if(mail($to,$subject,$message,$headers)) {
        echo "Mail Sent. Thank you " . $clientname . ", we will contact you shortly.";
    } else {
        echo "what";
    }
?>