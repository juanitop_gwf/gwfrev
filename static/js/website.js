$(document).on('keydown', `#cliContact, #contactfrom input[name='contact']`, function(e){
	-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
});
setTimeout(function () {
  $('#myModal').modal();
}, 84000);

$('.close').on("click",function(){
	$('#myModal').removeClass('show');
	$('#myModal').addClass('hide');
})

$(document).ready(function() {
  //timer protect 2018
	$("#protect-offer-timer")
  .countdown("2018/03/23", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer chinese new year
  $("#chinese-offer-timer")
  .countdown("2018/02/16", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer valentines
  $("#valentines-offer-timer")
  .countdown("2018/02/14", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer entrepreneur
  $("#entrepreneur-offer-timer")
  .countdown("2018/03/31", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer furniture
  $("#furniture-offer-timer")
  .countdown("2018/03/20", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer worldbex
  $("#worldbex-offer-timer")
  .countdown("2018/03/18", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
  //timer worldbex
  $("#manilafame-offer-timer")
  .countdown("2018/04/23", function(event) {
    $(this).html(
      event.strftime('<div class="timer-circle">'
        +'<p><span>%-D</span><br>DAYS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%H</span><br>HOURS</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%M</span><br>MINUTES</p></div>'
        +'<div class="timer-circle">'
        +'<p><span>%S</span><br>SECONDS</p></div></div>')
      )
    })
});