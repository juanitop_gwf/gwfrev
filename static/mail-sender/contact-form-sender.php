<?php
    header('Access-Control-Allow-Origin: *');
    $to = "inquiry@globalwebforce.com"; // this is your Email address
    $to2 = "inquiry@globalwebforce.com";
    $from = $_POST['cliEmail']; // this is the sender's Email address
    $clientname = $_POST['cliName'];
    $clientEmail = $_POST['cliEmail'];
    $clientContact = $_POST['cliContact'];
    $clientWebsite = $_POST['cliWebsite'];
    $package = $_POST['package'];
    $subject = 'Contact Form';
	$msg = $_POST['cliMsg'];
	$countryCode = $_POST['countryCode'];
	$address = "Unit 3203 Antel Global Corporate Center, Julia Vargas Ave., Ortigas Center, Pasig City, Philippines";
	$phone = "+63-917-568-0402";
	if($countryCode === 'AU') {
		$address = "425 Bell Street, Pascoe Vale South<br>(Corner of Springhall Parade),<br>Victoria, 3044, Australia";
		$phone = "0419 200 663";
	}
    $message = "<html><body>";
    $message .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
    $message .= "<tbody><tr>";
    $message .= "<td width='730' align='center' valign='top' style='padding-right:30px;padding-left:30px'>";
    $message .= "<img src='https://www.globalwebforce.com/static/img/logo_navi.png' alt=''></td></tr>";
    $message .= "<tr>";
    $message .= "<td align='left' valign='top' style='padding-top: 30px; padding-bottom: 30px; padding-right:30px;padding-left:30px; font-family: Arial;'>";
    $message .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='640' style='font-family: Arial;'>";
    $message .= "<tr>";
    $message .= "<td width='640px'>";
    $message .= "<h4>Inquiry</h4>";
    $message .= "<p>$msg</p>";
    $message .= "<h4>Client Info:</h4>";
    $message .= "<p><b>Name</b>: $clientname<br>";
    $message .= "<b>Contact</b>: $clientContact<br>";
    $message .= "<b>Email</b>: $clientEmail<br>";
    $message .= "<b>Website</b>: $clientWebsite</p>";
    $message .= "</td></tr></table></td></tr>";
    $message .= "<tr>";
    $message .= "<td align=center valign='top'  style='padding-right:30px;padding-left:30px;'>";
    $message .= "<table border='0' cellpadding='0' cellspacing='0' width='730'>";
    $message .= "<tbody><tr>";
    $message .= "<td bgcolor='#f17226' align='center' valign='top' style='font-size:12px; font-weight:400; line-height:24px; padding-top:20px; padding-bottom:20px; text-align:center; color:#fff;  font-family: Arial;'>";
    $message .= "<p style='color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center'>$address</p>";
    $message .= "<p style='color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center'>Phone: <a href='tel:".$phone."' style='color: #fff200; text-decoration: underline;'>$phone</a> | Email: <a href='mailto:inquiry@globalwebforce.com' style='color: #fff200; text-decoration: underline;'>inquiry@globalwebforce.com</a><br>";
    $message .= "Copyright ©2018 Global Web Force. All Rights Reserved.</p>";
    $message .= "<p>";
    $message .= "<a href='https://www.globalwebforce.com/' style='color:#fff;text-decoration:underline' target='_blank'>About Global WebForce | </a>";
    $message .= "<a href='https://www.globalwebforce.com/contact' style='color:#fff;text-decoration:underline' target='_blank'>Contact Us</a>";
    $message .= "</p>";
    $message .= "</td></tr></tbody></table></td></tr></tbody></table></body></html>";
    $headers = "From:" . $from."\r\n";
    $headers .= "Reply-To: ". $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    if(mail($to,$subject,$message,$headers)) {
        $autoResponder = "<html><body>";
		$autoResponder .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
		$autoResponder .= "<tbody><tr>";
		$autoResponder .= "<td width='730' align='center' valign='top' style='padding-right:30px;padding-left:30px'>";
		$autoResponder .= "<img src='https://www.globalwebforce.com/static/img/logo_navi.png' alt=''></td></tr>";
		$autoResponder .= "<tr>";
		$autoResponder .= "<td align='left' valign='top' style='padding-top: 30px; padding-bottom: 30px; padding-right:30px;padding-left:30px; font-family: Arial;'>";
		$autoResponder .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='640' style='font-family: Arial;'>";
		$autoResponder .= "<tr>";
		$autoResponder .= "<td width='640px'>";
		$autoResponder .= "<h4>Hi, $clientname</h4>";
		$autoResponder .= "<p>Thank you for reaching out through our contact form. A member of our team will reach out to you in no less than 24 hours.</p>";
		$autoResponder .= "<p>In the meantime, feel free to also call our hotline. <a href=tel:".$phone.">$phone</a></p>";
		$autoResponder .= "<p>Kind regards,</p>";
		$autoResponder .= "<h4>The GWF Team</h4>";
		$autoResponder .= "</td></tr></table></td></tr>";
		$autoResponder .= "<tr>";
		$autoResponder .= "<td align=center valign='top'  style='padding-right:30px;padding-left:30px;'>";
		$autoResponder .= "<table border='0' cellpadding='0' cellspacing='0' width='730'>";
		$autoResponder .= "<tbody><tr>";
		$autoResponder .= "<td bgcolor='#f17226' align='center' valign='top' style='font-size:12px; font-weight:400; line-height:24px; padding-top:20px; padding-bottom:20px; text-align:center; color:#fff;  font-family: Arial;'>";
		$autoResponder .= "<p style='color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center'>$address</p>";
		$autoResponder .= "<p style='color:#fff;font-size:12px;font-weight:400;line-height:24px;padding:0;margin:0;text-align:center'>Phone: <a href='tel:".$phone."' style='color: #fff200; text-decoration: underline;'>$phone</a> | Email: <a href='mailto:inquiry@globalwebforce.com' style='color: #fff200; text-decoration: underline;'>inquiry@globalwebforce.com</a><br>";
		$autoResponder .= "Copyright ©2018 Global Web Force. All Rights Reserved.</p>";
		$autoResponder .= "<p>";
		$autoResponder .= "<a href='https://www.globalwebforce.com/' style='color:#fff;text-decoration:underline' target='_blank'>About Global WebForce | </a>";
		$autoResponder .= "<a href='https://www.globalwebforce.com/contact' style='color:#fff;text-decoration:underline' target='_blank'>Contact Us</a>";
		$autoResponder .= "</p>";
		$autoResponder .= "</td></tr></tbody></table></td></tr></tbody></table></body></html>";
		$ARheaders = "From: inquiry@globalwebforce.com\r\n";
		$ARheaders .= "Reply-To: inquiry@globalwebforce.com\r\n";
		$ARheaders .= "MIME-Version: 1.0\r\n";
		$ARheaders .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($clientEmail,$subject,$autoResponder,$ARheaders);
    } else {
        echo "what";
    }
?>